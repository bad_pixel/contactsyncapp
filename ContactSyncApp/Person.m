//
//  Person.m
//  
//
//  Created by Piotr Pietrzak on 13.05.2013.
//  Copyright (c) 2013 DesDev. All rights reserved.
//

#import "Person.h"

@implementation Person

@synthesize fname;
@synthesize sname;

+ (Person *)instanceFromDictionary:(NSDictionary *)aDictionary
{
    Person *instance = [[Person alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary
{
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    [self setValuesForKeysWithDictionary:aDictionary];
}

- (NSDictionary *)getDictionaryWithAttributes
{
  return [NSDictionary dictionaryWithObjectsAndKeys:
          self.fname,
          @"fname",
          self.sname,
          @"sname",
          nil];
}

@end
