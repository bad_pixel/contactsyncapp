//
//  DDViewController.h
//  ContactSyncApp
//
//  Created by Piotr Pietrzak on 11.05.2013.
//  Copyright (c) 2013 desdev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import "DDContactTableViewCell.h"
#import <RestKit/RestKit.h>
#import "DDContactDataModel.h"
#import "Person.h"

@interface DDViewController : UIViewController <UITableViewDataSource>

@property (weak, nonatomic) DDContactDataModel *dataModel;
@property (weak, nonatomic) IBOutlet UIButton *sendButtonTouched;
@property (weak, nonatomic) IBOutlet UITableView *contactTableView;

@end
