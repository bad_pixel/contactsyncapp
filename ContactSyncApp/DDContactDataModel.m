//
//  DDContactDataModel.m
//  ContactSyncApp
//
//  Created by Piotr Pietrzak on 13.05.2013.
//  Copyright (c) 2013 desdev. All rights reserved.
//

#import "DDContactDataModel.h"

@implementation DDContactDataModel

@synthesize tableData;

+ (id)sharedDDContactDataModel
{
  static dispatch_once_t onceQueue;
  static DDContactDataModel *dDContactDataModel = nil;
  
  dispatch_once(&onceQueue, ^{ dDContactDataModel = [[self alloc] init]; });
  return dDContactDataModel;
}

@end
