//
//  DDContactDataModel.h
//  ContactSyncApp
//
//  Created by Piotr Pietrzak on 13.05.2013.
//  Copyright (c) 2013 desdev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDContactDataModel : NSObject

@property (nonatomic, strong) NSMutableArray *tableData;

+ (id)sharedDDContactDataModel;

@end
