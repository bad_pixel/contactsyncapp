//
//  main.m
//  ContactSyncApp
//
//  Created by Piotr Pietrzak on 11.05.2013.
//  Copyright (c) 2013 desdev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DDAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([DDAppDelegate class]));
  }
}
