//
//  DDContactTableViewCell.m
//  ContactSyncApp
//
//  Created by Piotr Pietrzak on 13.05.2013.
//  Copyright (c) 2013 desdev. All rights reserved.
//

#import "DDContactTableViewCell.h"

@implementation DDContactTableViewCell

@synthesize fnameLabel = _fnameLabel;
@synthesize snameLabel = _snameLabel;
@synthesize checkboxImageView = _checkboxImageView;
@synthesize index = _index;
@synthesize locked = _locked;

- (void)loadContentFromIndex:(NSUInteger)index
{
  self.activityIndicator.hidden = YES;
  if (self.dataModel == nil) {
    self.dataModel = [DDContactDataModel sharedDDContactDataModel];
  }
  self.index = index;
  Person *person = [self.dataModel.tableData objectAtIndex:index];
  self.fnameLabel.text = person.fname;
  self.snameLabel.text = person.sname;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
  if (!self.locked) {
    [super setSelected:selected animated:animated];
    self.checkboxImageView.image = (selected) ? [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Checkbox_checked" ofType:@"png"]] : [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Checkbox_unchecked" ofType:@"png"]];
  }
}

- (void)updateCell
{
  NSURL *url = [NSURL URLWithString:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"requestPath"]];

  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[(Person *)[self.dataModel.tableData objectAtIndex:self.index] getDictionaryWithAttributes] options:kNilOptions error:nil];
    
  NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:url];
  [request setHTTPMethod:@"POST"];
  [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [request setValue:@"json" forHTTPHeaderField:@"Data-Type"];
  [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
  [request setHTTPBody: jsonData];
  
  self.activityIndicator.hidden = false;
  self.checkboxImageView.hidden = true;
  self.locked = YES;
  
  AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSDictionary *JSON) {
    [(Person *)[self.dataModel.tableData objectAtIndex:self.index] setAttributesFromDictionary:JSON];
    [self loadContentFromIndex:self.index];
    self.locked = NO;
    self.activityIndicator.hidden = true;
    self.checkboxImageView.hidden = false;
    self.selected = false;
  } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
    NSLog(@"%@", [error userInfo]);
    self.locked = NO;
    self.activityIndicator.hidden = true;
    self.checkboxImageView.hidden = false;
  }];
  
  [operation start];
}

@end
