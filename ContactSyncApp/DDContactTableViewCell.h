//
//  DDContactTableViewCell.h
//  ContactSyncApp
//
//  Created by Piotr Pietrzak on 13.05.2013.
//  Copyright (c) 2013 desdev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDContactDataModel.h"
#import "Person.h"
#import <RestKit/RestKit.h>

@interface DDContactTableViewCell : UITableViewCell

@property (nonatomic, weak) DDContactDataModel *dataModel;
@property (nonatomic, weak) IBOutlet UILabel *fnameLabel;
@property (nonatomic, weak) IBOutlet UILabel *snameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *checkboxImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) BOOL locked;
@property (nonatomic) NSInteger index;

- (void)loadContentFromIndex:(NSUInteger)index;

- (void)updateCell;

@end
