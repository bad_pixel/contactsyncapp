//
//  DDAppDelegate.h
//  ContactSyncApp
//
//  Created by Piotr Pietrzak on 11.05.2013.
//  Copyright (c) 2013 desdev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>

@class DDViewController;

@interface DDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DDViewController *viewController;

@end
