//
//  Person.h
//  
//
//  Created by Piotr Pietrzak on 13.05.2013.
//  Copyright (c) 2013 DesDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject {
    NSString *fname;
    NSString *sname;
}

@property (nonatomic, copy) NSString *fname;
@property (nonatomic, copy) NSString *sname;

+ (Person *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;
- (NSDictionary *)getDictionaryWithAttributes;
@end
