//
//  DDViewController.m
//  ContactSyncApp
//
//  Created by Piotr Pietrzak on 11.05.2013.
//  Copyright (c) 2013 desdev. All rights reserved.
//

#import "DDViewController.h"

@implementation DDViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  self.dataModel = [DDContactDataModel sharedDDContactDataModel];
  self.dataModel.tableData = [[NSMutableArray alloc] init];
  [self getPersonListOutOfAddressBook];
}

#pragma mark - UiTableViewDataSource protocol implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [self.dataModel.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *cellIdentifier = @"DDContactTableViewCell";
  DDContactTableViewCell *cell = (DDContactTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
  if (cell == nil) {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DDContactTableViewCell"
                                                 owner:self
                                               options:nil];
    cell = [nib objectAtIndex:0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
  }
  [cell loadContentFromIndex:(NSInteger)indexPath.row];
  return cell;
}

#pragma mark - Actions

- (IBAction)sendButtonTouched:(UIButton *)sender
{
  for (DDContactTableViewCell *cell in self.contactTableView.visibleCells) {
    if (cell.selected && !cell.locked) {
      [cell updateCell];
    }
  }
}

#pragma mark - Other methods

- (void)getPersonListOutOfAddressBook
{
  CFErrorRef error = NULL;
  ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
  
  if (addressBook != nil) {
    NSArray *allContacts = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    for (NSUInteger i = 0; i < 5; i++) {
      ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
      NSDictionary *dataDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                      (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty),
                                      @"fname",
                                      (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonLastNameProperty),
                                      @"sname",
                                      nil];
      
      Person *person = [Person instanceFromDictionary:dataDictionary];
      [self.dataModel.tableData addObject:person];
    }
    CFRelease(addressBook);
  } else {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Błąd"
                                                    message:@"Nie można załadować książki adresowej."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
  }
}

@end
